`timescale 1ns / 1ps

module test_rx (
  input wire RESET,
  input wire RX_CLK_IN,
  input wire [31:0] RX_DATA_IN,
  input wire [3:0]  RX_CHARISK_IN,
  input wire [3:0]  RX_DISPERR_IN,
  output reg [31:0] DECODED_DATA_OUT,
  output reg [3:0]  DECODED_CHARISK_OUT,
  output reg COMMA_PULSE_OUT,
  output reg LOCK_OUT,
  output reg RXSLIDE_OUT,
  input wire RXRESETDONE,
  output reg RXRERESET
  );

  reg [3:0] counter_i = 4'b0;
  reg waiting_i = 1'b1;
  reg [5:0] wait_counter_i = 6'b0;
  reg [8:0] lock_counter_i = 9'b0;
  reg [7:0] rxslide_counter_i = 8'b0;
  reg rxslide_i = 1'b0;
  reg rxcommadet_i;
  reg rx_rereset_i = 1'b0;
  reg [5:0] rx_rereset_counter_i = 6'b0;
  reg [9:0] rx_10bit_data_0_i;
  reg [9:0] rx_10bit_data_1_i;
  reg [9:0] rx_10bit_data_2_i;
  reg [9:0] rx_10bit_data_3_i;
  wire [7:0] rx_8bit_data_0_i;
  wire [7:0] rx_8bit_data_1_i;
  wire [7:0] rx_8bit_data_2_i;
  wire [7:0] rx_8bit_data_3_i;
  wire rx_isk_0_i;
  wire rx_isk_1_i;
  wire rx_isk_2_i;
  wire rx_isk_3_i;

  parameter mcommaword = 10'b1010000011;
  parameter pcommaword = 10'b0101111100;
  
  always @(posedge RX_CLK_IN) begin
    RXSLIDE_OUT <= rxslide_i;
    LOCK_OUT <= &lock_counter_i;
    DECODED_DATA_OUT <= {rx_8bit_data_3_i, rx_8bit_data_2_i, rx_8bit_data_1_i, rx_8bit_data_0_i};
    DECODED_CHARISK_OUT <= {rx_isk_3_i, rx_isk_2_i, rx_isk_1_i, rx_isk_0_i};
    COMMA_PULSE_OUT <= rxcommadet_i;
    RXRERESET <= rx_rereset_i;
  end
   
  always @(posedge RX_CLK_IN) begin
    if (rx_rereset_i == 1'b0) begin
      if (LOCK_OUT == 1'b1 && rxslide_counter_i[0] == 1'b0) begin
        rx_rereset_i <= 1'b1;
        rx_rereset_counter_i <= 6'b0;
      end
    end
    else begin
      if (&rx_rereset_counter_i == 1'b1) begin
        rx_rereset_i <= 1'b0;
        //rx_rereset_counter_i <= 6'b0;
      end
      else begin
        rx_rereset_counter_i <= rx_rereset_counter_i + 6'b1;
      end
    end
  end

  always @(posedge RX_CLK_IN) begin
    if (RESET == 1'b1 || RXRESETDONE == 1'b0) begin
      counter_i <= 4'b0;
      rxcommadet_i <= 1'b0;
      lock_counter_i <= 9'b0;
      rxslide_i <= 1'b0;
      rxslide_counter_i <= 8'b0;
    end

    else begin
      rx_10bit_data_3_i[7:0] <= RX_DATA_IN[31:24];
      rx_10bit_data_3_i[8]   <= RX_CHARISK_IN[3];
      rx_10bit_data_3_i[9]   <= RX_DISPERR_IN[3];

      rx_10bit_data_2_i[7:0] <= RX_DATA_IN[23:16];
      rx_10bit_data_2_i[8]   <= RX_CHARISK_IN[2];
      rx_10bit_data_2_i[9]   <= RX_DISPERR_IN[2];

      rx_10bit_data_1_i[7:0] <= RX_DATA_IN[15:8];
      rx_10bit_data_1_i[8]   <= RX_CHARISK_IN[1];
      rx_10bit_data_1_i[9]   <= RX_DISPERR_IN[1];

      rx_10bit_data_0_i[7:0] <= RX_DATA_IN[7:0];
      rx_10bit_data_0_i[8]   <= RX_CHARISK_IN[0];
      rx_10bit_data_0_i[9]   <= RX_DISPERR_IN[0];

      if ((rx_10bit_data_0_i == mcommaword) || (rx_10bit_data_0_i == pcommaword)) begin // comma
        rxcommadet_i <= 1'b1;
        rxslide_i <= 1'b0;
        counter_i <= 4'b0;

        if ((counter_i == 4'h7) && (&lock_counter_i == 1'b0)) begin
          lock_counter_i <= lock_counter_i + 9'b1;
        end

        if (waiting_i == 1'b1) begin
          if(&wait_counter_i == 1'b1) begin
            waiting_i <= 1'b0;
            wait_counter_i <= 6'b0;
          end
          else begin
            wait_counter_i <= wait_counter_i +6'b1;
          end
        end
      end
      else if (counter_i == 4'h8) begin
        counter_i <= 4'b0;
        rxcommadet_i <= 1'b0;
        lock_counter_i <= 9'b0;

        if (waiting_i == 1'b0) begin
          rxslide_i <= 1'b1;
          waiting_i <= 1'b1;
          rxslide_counter_i <= rxslide_counter_i + 8'b1;
        end
        else begin
          rxslide_i <= 1'b0;

          if (&wait_counter_i == 1'b1) begin
            waiting_i <= 1'b0;
            wait_counter_i <= 6'b0;
          end
          else begin
            wait_counter_i <= wait_counter_i + 6'b1;
          end
        end
      end
      else begin
        rxcommadet_i <= 1'b0;
        rxslide_i <= 1'b0;
        counter_i <= counter_i + 4'b1;

        if(waiting_i == 1'b1) begin
          if(&wait_counter_i == 1'b1) begin
            waiting_i <= 1'b0;
            wait_counter_i <= 6'b0;
          end
          else begin
            wait_counter_i <= wait_counter_i + 6'b1;
          end
        end
      end
    end
  end

  dec_8b10b dec_8b10b_byte_0_i (
    .RESET (RESET),
    .RBYTECLK (RX_CLK_IN),
    .AI (rx_10bit_data_0_i[0]),
    .BI (rx_10bit_data_0_i[1]),
    .CI (rx_10bit_data_0_i[2]),
    .DI (rx_10bit_data_0_i[3]),
    .EI (rx_10bit_data_0_i[4]),
    .II (rx_10bit_data_0_i[5]),
    .FI (rx_10bit_data_0_i[6]),
    .GI (rx_10bit_data_0_i[7]),
    .HI (rx_10bit_data_0_i[8]),
    .JI (rx_10bit_data_0_i[9]),
    .KO (rx_isk_0_i),
    .HO (rx_8bit_data_0_i[7]),
    .GO (rx_8bit_data_0_i[6]),
    .FO (rx_8bit_data_0_i[5]),
    .EO (rx_8bit_data_0_i[4]),
    .DO (rx_8bit_data_0_i[3]),
    .CO (rx_8bit_data_0_i[2]),
    .BO (rx_8bit_data_0_i[1]),
    .AO (rx_8bit_data_0_i[0])
  );

  dec_8b10b dec_8b10b_byte_1_i (
    .RESET (RESET),
    .RBYTECLK (RX_CLK_IN),
    .AI (rx_10bit_data_1_i[0]),
    .BI (rx_10bit_data_1_i[1]),
    .CI (rx_10bit_data_1_i[2]),
    .DI (rx_10bit_data_1_i[3]),
    .EI (rx_10bit_data_1_i[4]),
    .II (rx_10bit_data_1_i[5]),
    .FI (rx_10bit_data_1_i[6]),
    .GI (rx_10bit_data_1_i[7]),
    .HI (rx_10bit_data_1_i[8]),
    .JI (rx_10bit_data_1_i[9]),
    .KO (rx_isk_1_i),
    .HO (rx_8bit_data_1_i[7]),
    .GO (rx_8bit_data_1_i[6]),
    .FO (rx_8bit_data_1_i[5]),
    .EO (rx_8bit_data_1_i[4]),
    .DO (rx_8bit_data_1_i[3]),
    .CO (rx_8bit_data_1_i[2]),
    .BO (rx_8bit_data_1_i[1]),
    .AO (rx_8bit_data_1_i[0])
  );

  dec_8b10b dec_8b10b_byte_2_i (
    .RESET (RESET),
    .RBYTECLK (RX_CLK_IN),
    .AI (rx_10bit_data_2_i[0]),
    .BI (rx_10bit_data_2_i[1]),
    .CI (rx_10bit_data_2_i[2]),
    .DI (rx_10bit_data_2_i[3]),
    .EI (rx_10bit_data_2_i[4]),
    .II (rx_10bit_data_2_i[5]),
    .FI (rx_10bit_data_2_i[6]),
    .GI (rx_10bit_data_2_i[7]),
    .HI (rx_10bit_data_2_i[8]),
    .JI (rx_10bit_data_2_i[9]),
    .KO (rx_isk_2_i),
    .HO (rx_8bit_data_2_i[7]),
    .GO (rx_8bit_data_2_i[6]),
    .FO (rx_8bit_data_2_i[5]),
    .EO (rx_8bit_data_2_i[4]),
    .DO (rx_8bit_data_2_i[3]),
    .CO (rx_8bit_data_2_i[2]),
    .BO (rx_8bit_data_2_i[1]),
    .AO (rx_8bit_data_2_i[0])
  );

  dec_8b10b dec_8b10b_byte_3_i (
    .RESET (RESET),
    .RBYTECLK (RX_CLK_IN),
    .AI (rx_10bit_data_3_i[0]),
    .BI (rx_10bit_data_3_i[1]),
    .CI (rx_10bit_data_3_i[2]),
    .DI (rx_10bit_data_3_i[3]),
    .EI (rx_10bit_data_3_i[4]),
    .II (rx_10bit_data_3_i[5]),
    .FI (rx_10bit_data_3_i[6]),
    .GI (rx_10bit_data_3_i[7]),
    .HI (rx_10bit_data_3_i[8]),
    .JI (rx_10bit_data_3_i[9]),
    .KO (rx_isk_3_i),
    .HO (rx_8bit_data_3_i[7]),
    .GO (rx_8bit_data_3_i[6]),
    .FO (rx_8bit_data_3_i[5]),
    .EO (rx_8bit_data_3_i[4]),
    .DO (rx_8bit_data_3_i[3]),
    .CO (rx_8bit_data_3_i[2]),
    .BO (rx_8bit_data_3_i[1]),
    .AO (rx_8bit_data_3_i[0])
  );

endmodule

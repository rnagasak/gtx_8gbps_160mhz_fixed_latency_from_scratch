library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity test_tx is
  port
    (
      CLK_IN : in std_logic;
      COMMA_PULSE_OUT : out std_logic;
      TX_DATA_OUT : out std_logic_vector(31 downto 0);
      TX_CHARISK_OUT : out std_logic_vector(3 downto 0)
      );
end test_tx;


architecture RTL of test_tx is

  signal counter_i : std_logic_vector(2 downto 0) := (others => '0');
  signal random_data_i : std_logic_vector(31 downto 0) := (others => '0');
  signal is_comma_i : std_logic;
  
begin  -- RTL
  
  COMMA_PULSE_OUT <= is_comma_i;
  
  process (CLK_IN)
  begin
    if rising_edge(CLK_IN) then
      counter_i <= counter_i + 1;
      if (or_reduce(counter_i) = '0') then
        TX_DATA_OUT <= x"000000bc";
        TX_CHARISK_OUT <= "0001";
        is_comma_i <= '1';
      else
        TX_DATA_OUT <= random_data_i;
        TX_CHARISK_OUT <= "0000";
        random_data_i <= random_data_i + 1;
        is_comma_i <= '0';
      end if;
    end if;
  end process;
  

end RTL;

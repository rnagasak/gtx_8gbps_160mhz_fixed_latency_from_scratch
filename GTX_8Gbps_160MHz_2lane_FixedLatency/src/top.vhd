library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;
use ieee.std_logic_1164.all;
library UNISIM;
use UNISIM.VCOMPONENTS.all;
library customized_ip;


entity top is
  port
    (
      LED0    : out std_logic;
      LED1    : out std_logic;
      LED2    : out std_logic;      
      LED3    : out std_logic;
      -- BANK 33
      SYSCLKP : in  std_logic;
      SYSCLKN : in  std_logic;
      LEMOOUT : out std_logic;  

      -- BANK 111
      GTXTXP1 : out std_logic;
      GTXRXP1 : in  std_logic;
      GTXTXN1 : out std_logic;
      GTXRXN1 : in  std_logic;
      -- BANK 112
      GTXTXP0 : out std_logic;
      GTXRXP0 : in  std_logic;
      GTXTXN0 : out std_logic;
      GTXRXN0 : in  std_logic;

      Q2_CLK1_GTREFCLK_PAD_N_IN : in  std_logic;
      Q2_CLK1_GTREFCLK_PAD_P_IN : in  std_logic;
      PRESETB                   : in  std_logic
      );
  
end top;

architecture RTL of top is
  
  signal drp_clk_i                     : std_logic;

  signal tx0_userclk_i                 : std_logic;
  signal tx0_userclk_bufg_i                 : std_logic;
  signal rx0_userclk_i                 : std_logic;
  signal tx1_userclk_i                 : std_logic;
  signal rx1_userclk_i                 : std_logic;
  
  signal tx0_data_i                    : std_logic_vector(31 downto 0);
  signal tx0_charisk_i                 : std_logic_vector(3 downto 0);
  signal tx1_data_i                    : std_logic_vector(31 downto 0);
  signal tx1_charisk_i                 : std_logic_vector(3 downto 0);

  signal rx0_data_before_decoding_i                    : std_logic_vector(31 downto 0);
  signal rx0_charisk_before_decoding_i                 : std_logic_vector(3 downto 0);
  signal rx0_disperr_before_decoding_i                 : std_logic_vector(3 downto 0);
  signal rx0_decoded_data_i                    : std_logic_vector(31 downto 0);
  signal rx0_decoded_charisk_i                 : std_logic_vector(3 downto 0);

  signal rx1_data_before_decoding_i                    : std_logic_vector(31 downto 0);
  signal rx1_charisk_before_decoding_i                 : std_logic_vector(3 downto 0);
  signal rx1_disperr_before_decoding_i                 : std_logic_vector(3 downto 0);
  signal rx1_decoded_data_i                    : std_logic_vector(31 downto 0);
  signal rx1_decoded_charisk_i                 : std_logic_vector(3 downto 0);
  
  signal rx0_slide_i                    : std_logic;
  signal rx1_slide_i                    : std_logic;

  signal rx0_locked_i                   : std_logic;
  signal rx1_locked_i                   : std_logic;

  signal clk_wiz_0_locked_i            : std_logic;
  signal por_i                         : std_logic;

  signal rx_soft_reset_i              : std_logic;
  signal tx_soft_reset_i               : std_logic;

  signal rx0_resetdone_i                : std_logic;
  signal rx1_resetdone_i                : std_logic;
  signal rx0_rereset_i                   : std_logic;
  signal rx1_rereset_i                   : std_logic;
  
  signal tx0_comma_pulse_out_i          : std_logic;
  signal tx1_comma_pulse_out_i          : std_logic;
  signal rx0_comma_pulse_out_i          : std_logic;
  signal rx1_comma_pulse_out_i          : std_logic;

  signal tx0_resetdone_i	       : std_logic;
  signal tx0_fsm_resetdone_i      : std_logic;
  signal rx0_fsm_resetdone_i      : std_logic;
  signal tx1_resetdone_i	       : std_logic;
  signal tx1_fsm_resetdone_i      : std_logic;
  signal rx1_fsm_resetdone_i      : std_logic;

  signal rx0_byteisaligned_i       : std_logic;
  signal rx0_realign_i             : std_logic;
  signal rx0_commadet_i            : std_logic;
  signal rx1_byteisaligned_i       : std_logic;
  signal rx1_realign_i             : std_logic;
  signal rx1_commadet_i            : std_logic;

  signal reset0_i                   : std_logic;
  
  signal all_reset_done_i              : std_logic;
  signal reset_vio_0_i     : std_logic;
  
  COMPONENT ila_0

PORT (
	clk : IN STD_LOGIC;

	probe0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	probe1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)
);
END COMPONENT  ;

COMPONENT vio_0
  PORT (
    clk : IN STD_LOGIC;
    probe_out0 : OUT STD_LOGIC
  );
END COMPONENT;

  component test_tx
    port
      (
        CLK_IN          : in std_logic;
        COMMA_PULSE_OUT : out std_logic;        
        TX_DATA_OUT     : out std_logic_vector(31 downto 0);
        TX_CHARISK_OUT  : out std_logic_vector(3 downto 0)
        );
  end component;

component test_rx
  --component rx_comma_detection                                                            
    port
      (
        RESET : in std_logic;
        RX_CLK_IN : in std_logic;
        RX_DATA_IN : in std_logic_vector(31 downto 0);
        RX_CHARISK_IN : in std_logic_vector(3 downto 0);
        RX_DISPERR_IN : in std_logic_vector(3 downto 0);
        DECODED_DATA_OUT : out std_logic_vector(31 downto 0);
        DECODED_CHARISK_OUT : out std_logic_vector(3 downto 0);
        --DECODED_ISCOMMA_OUT : out std_logic_vector(3 downto 0);                           
        COMMA_PULSE_OUT : out std_logic;
        LOCK_OUT : out std_logic;
        RXSLIDE_OUT : out std_logic;
        RXRESETDONE : in std_logic;
        RXRERESET : out std_logic
        );
  end component;

  component clk_wiz_0
    port ( 
      clk_out1  : out std_logic;
      reset     : in std_logic;
      locked    : out std_logic;
      clk_in1_p : in std_logic;
      clk_in1_n : in std_logic
      );
  end component;

  component gtwizard_0_exdes
    port
      (
        Q2_CLK1_GTREFCLK_PAD_N_IN               : in   std_logic;
        Q2_CLK1_GTREFCLK_PAD_P_IN               : in   std_logic;
        DRPCLK_IN                               : in   std_logic;
        SOFT_RESET_TX_IN                        : in   std_logic;
        SOFT_RESET_RX_IN                        : in   std_logic;

        GT0_TX_USERCLK_OUT                          : out  std_logic;
        GT0_RX_USERCLK_OUT                          : out  std_logic;
        GT0_TX_DATA_IN                              : in   std_logic_vector(31 downto 0);
        GT0_TX_CHARISK_IN                           : in   std_logic_vector(3 downto 0);
        GT0_RX_DATA_OUT                             : out  std_logic_vector(31 downto 0);
        GT0_RX_CHARISK_OUT                          : out  std_logic_vector(3 downto 0);
        GT0_RX_DISPERR_OUT                          : out  std_logic_vector(3 downto 0);
        GT0_RXSLIDE_IN                              : in   std_logic;
        GT0_RX_BYTEISALIGNED_OUT                    : out  std_logic;
        GT0_RX_REALIGN_OUT                          : out  std_logic;
        GT0_RX_COMMADET_OUT                         : out  std_logic;
        GT0_TX_RESETDONE_OUT                        : out  std_logic;
        GT0_RX_RESETDONE_OUT                        : out  std_logic;
        GT0_TX_FSM_RESET_DONE_OUT                   : out  std_logic;
        GT0_RX_FSM_RESET_DONE_OUT                   : out  std_logic;

        GT1_TX_USERCLK_OUT                          : out  std_logic;
        GT1_RX_USERCLK_OUT                          : out  std_logic;
        GT1_TX_DATA_IN                              : in   std_logic_vector(31 downto 0);
        GT1_TX_CHARISK_IN                           : in   std_logic_vector(3 downto 0);
        GT1_RX_DATA_OUT                             : out  std_logic_vector(31 downto 0);
        GT1_RX_CHARISK_OUT                          : out  std_logic_vector(3 downto 0);
        GT1_RX_DISPERR_OUT                          : out  std_logic_vector(3 downto 0);
        GT1_RXSLIDE_IN                              : in   std_logic;
        GT1_RX_BYTEISALIGNED_OUT                    : out  std_logic;
        GT1_RX_REALIGN_OUT                          : out  std_logic;
        GT1_RX_COMMADET_OUT                         : out  std_logic;
        GT1_TX_RESETDONE_OUT                        : out  std_logic;
        GT1_RX_RESETDONE_OUT                        : out  std_logic;
        GT1_TX_FSM_RESET_DONE_OUT                   : out  std_logic;
        GT1_RX_FSM_RESET_DONE_OUT                   : out  std_logic;

        RXN_IN                                  : in   std_logic_vector(1 downto 0);
        RXP_IN                                  : in   std_logic_vector(1 downto 0);
        TXN_OUT                                 : out  std_logic_vector(1 downto 0);
        TXP_OUT                                 : out  std_logic_vector(1 downto 0)
        );
  end component;

begin

  por_i           <= not clk_wiz_0_locked_i;      -- power on reset
  tx_soft_reset_i <=  (not PRESETB) or por_i or reset_vio_0_i;
  rx_soft_reset_i <=  (not PRESETB) or por_i or rx0_rereset_i or rx1_rereset_i or reset_vio_0_i; 

  LED0 <= rx0_byteisaligned_i and rx1_byteisaligned_i;
  
  ila_0_i : ila_0
PORT MAP (
	clk => tx0_userclk_i,

	probe0 => tx0_data_i,
	probe1 => tx1_data_i
);

vio_0_i : vio_0
  PORT MAP (
    clk => tx0_userclk_i,
    probe_out0 => reset_vio_0_i
  );


  clk_wiz_0_i : clk_wiz_0
    port map ( 
      clk_out1  => drp_clk_i,
      reset     => '0',
      locked    => clk_wiz_0_locked_i,
      clk_in1_p => SYSCLKP,
      clk_in1_n => SYSCLKN); 

  gtwizard_0_exdes_i : gtwizard_0_exdes
    port map (
      Q2_CLK1_GTREFCLK_PAD_N_IN               =>  Q2_CLK1_GTREFCLK_PAD_N_IN,
      Q2_CLK1_GTREFCLK_PAD_P_IN               =>  Q2_CLK1_GTREFCLK_PAD_P_IN,
      DRPCLK_IN                               => drp_clk_i,
      SOFT_RESET_TX_IN                        => tx_soft_reset_i,
      SOFT_RESET_RX_IN                        => rx_soft_reset_i,

      GT0_TX_USERCLK_OUT                          => tx0_userclk_i,
      GT0_RX_USERCLK_OUT                          => rx0_userclk_i,
      GT0_TX_DATA_IN                              => tx0_data_i,
      GT0_TX_CHARISK_IN                           => tx0_charisk_i,
      GT0_RX_DATA_OUT                             => rx0_data_before_decoding_i,
      GT0_RX_CHARISK_OUT                          => rx0_charisk_before_decoding_i,
      GT0_RX_DISPERR_OUT                          => rx0_disperr_before_decoding_i,
      GT0_RXSLIDE_IN                              => rx0_slide_i,
      GT0_RX_BYTEISALIGNED_OUT                    => rx0_byteisaligned_i,
      GT0_RX_REALIGN_OUT                          => rx0_realign_i,
      GT0_RX_COMMADET_OUT                         => rx0_commadet_i,
      GT0_TX_RESETDONE_OUT                        => tx0_resetdone_i,
      GT0_RX_RESETDONE_OUT                        => rx0_resetdone_i,
      GT0_TX_FSM_RESET_DONE_OUT                   => tx0_fsm_resetdone_i,
      GT0_RX_FSM_RESET_DONE_OUT                   => rx0_fsm_resetdone_i,

      GT1_TX_USERCLK_OUT                          => tx1_userclk_i,
      GT1_RX_USERCLK_OUT                          => rx1_userclk_i,
      GT1_TX_DATA_IN                              => tx1_data_i,
      GT1_TX_CHARISK_IN                           => tx1_charisk_i,
      GT1_RX_DATA_OUT                             => rx1_data_before_decoding_i,
      GT1_RX_CHARISK_OUT                          => rx1_charisk_before_decoding_i,
      GT1_RX_DISPERR_OUT                          => rx1_disperr_before_decoding_i,
      GT1_RXSLIDE_IN                              => rx1_slide_i,
      GT1_RX_BYTEISALIGNED_OUT                    => rx1_byteisaligned_i,
      GT1_RX_REALIGN_OUT                          => rx1_realign_i,
      GT1_RX_COMMADET_OUT                         => rx1_commadet_i,
      GT1_TX_RESETDONE_OUT                        => tx1_resetdone_i,
      GT1_RX_RESETDONE_OUT                        => rx1_resetdone_i,
      GT1_TX_FSM_RESET_DONE_OUT                   => tx1_fsm_resetdone_i,
      GT1_RX_FSM_RESET_DONE_OUT                   => rx1_fsm_resetdone_i,
      
      
      RXN_IN(0)    	        => GTXRXN0,
      RXN_IN(1)    	        => GTXRXN1,
      RXP_IN(0)    	        => GTXRXP0, 
      RXP_IN(1)    	        => GTXRXP1, 
      TXN_OUT(0)   	        => GTXTXN0,
      TXN_OUT(1)   	        => GTXTXN1,
      TXP_OUT(0)   	        => GTXTXP0,
      TXP_OUT(1)   	        => GTXTXP1
      );

  
  test_tx0_i : test_tx
    port map (
      CLK_IN          => tx0_userclk_i,
      COMMA_PULSE_OUT => tx0_comma_pulse_out_i,      
      TX_DATA_OUT     => tx0_data_i,
      TX_CHARISK_OUT  => tx0_charisk_i);

  test_tx1_i : test_tx
    port map (
      CLK_IN          => tx1_userclk_i,
      COMMA_PULSE_OUT => tx1_comma_pulse_out_i, -- tx_comma_pulse_out_i,      
      TX_DATA_OUT     => tx1_data_i,
      TX_CHARISK_OUT  => tx1_charisk_i);

test_rx0_i : test_rx
  --rx_comma_detection_i : rx_comma_detection                                               
    port map(
      RESET => rx_soft_reset_i,
      RX_CLK_IN => rx0_userclk_i,
      RX_DATA_IN => rx0_data_before_decoding_i,
      RX_CHARISK_IN => rx0_charisk_before_decoding_i,
      RX_DISPERR_IN => rx0_disperr_before_decoding_i,
      DECODED_DATA_OUT => rx0_decoded_data_i,
      DECODED_CHARISK_OUT => rx0_decoded_charisk_i,
      --DECODED_ISCOMMA_OUT => open,                                                        
      COMMA_PULSE_OUT => rx0_comma_pulse_out_i,
      LOCK_OUT => rx0_locked_i,
      RXSLIDE_OUT => rx0_slide_i,
      RXRESETDONE => rx0_resetdone_i,
      RXRERESET => rx0_rereset_i);

  test_rx1_i : test_rx
  --rx_comma_detection_i : rx_comma_detection                                               
    port map(
      RESET => rx_soft_reset_i,
      RX_CLK_IN => rx1_userclk_i,
      RX_DATA_IN => rx1_data_before_decoding_i,
      RX_CHARISK_IN => rx1_charisk_before_decoding_i,
      RX_DISPERR_IN => rx1_disperr_before_decoding_i,
      DECODED_DATA_OUT => rx1_decoded_data_i,
      DECODED_CHARISK_OUT => rx1_decoded_charisk_i,
      --DECODED_ISCOMMA_OUT => open,                                                        
      COMMA_PULSE_OUT => rx1_comma_pulse_out_i,
      LOCK_OUT => rx1_locked_i,
      RXSLIDE_OUT => rx1_slide_i,
      RXRESETDONE => rx1_resetdone_i,
      RXRERESET => rx1_rereset_i);

  process (rx0_userclk_i)
  begin
    if rising_edge(rx0_userclk_i) then
      if (rx0_byteisaligned_i = '0') then
        LED1 <= '0';
        LED2 <= '0';
        LED3 <= '0';
      elsif (rx0_decoded_charisk_i(0)='0') then
        LED1 <= rx0_decoded_data_i(29);
        LED2 <= rx0_decoded_data_i(30);
        LED3 <= rx0_decoded_data_i(31);
      end if;
    end if;
  end process;

--   process (rx1_userclk_i)
--   begin
--     if rising_edge(rx1_userclk_i) then
--       if (rx1_byteisaligned_i = '0') then
--         LED1 <= '0';
--         LED2 <= '0';
--         LED3 <= '0';
--       elsif (rx1_decoded_charisk_i(0)='0') then
--         LED1 <= rx1_decoded_data_i(29);
--         LED2 <= rx1_decoded_data_i(30);
--         LED3 <= rx1_decoded_data_i(31);
--       end if;
--     end if;
--   end process;
  
  LEMOOUT <= tx0_comma_pulse_out_i;

  
end RTL;

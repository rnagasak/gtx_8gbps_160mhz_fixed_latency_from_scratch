# I/O assignment for each bank of FPGA

#Bank13
set_property PACKAGE_PIN AA14 [get_ports PRESETB]
set_property IOSTANDARD LVCMOS33 [get_ports PRESETB]

# BANK 11
set_property PACKAGE_PIN AK20 [get_ports LED0]
set_property PACKAGE_PIN AJ19 [get_ports LED1]
set_property PACKAGE_PIN AK18 [get_ports LED2]
set_property PACKAGE_PIN AK17 [get_ports LED3]

set_property IOSTANDARD LVCMOS33 [get_ports LED0]
set_property IOSTANDARD LVCMOS33 [get_ports LED1]
set_property IOSTANDARD LVCMOS33 [get_ports LED2]
set_property IOSTANDARD LVCMOS33 [get_ports LED3]

# BANK 33
set_property PACKAGE_PIN G5 [get_ports SYSCLKP]
set_property PACKAGE_PIN G4 [get_ports SYSCLKN]

set_property IOSTANDARD LVDS [get_ports SYSCLKP]
set_property IOSTANDARD LVDS [get_ports SYSCLKN]

set_property PACKAGE_PIN F4 [get_ports LEMOOUT]
set_property IOSTANDARD LVCMOS18 [get_ports LEMOOUT]


set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]

# moved from gtwizard_0_exdes.xdc

 create_clock -period 6.25 [get_ports Q2_CLK1_GTREFCLK_PAD_P_IN]
 create_clock -name drpclk_in_i -period 5.000 [get_ports SYSCLK_P]
 set_false_path -to [get_pins -filter {REF_PIN_NAME=~*CLR} -of_objects [get_cells -hierarchical -filter \
{NAME =~ *_txfsmresetdone_r*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*D} -of_objects [get_cells -hierarchical -filter {N\
AME =~ *_txfsmresetdone_r*}]]
set_false_path -to [get_pins -filter {REF_PIN_NAME=~*D} -of_objects [get_cells -hierarchical -filter {N\
AME =~ *reset_on_error_in_r*}]]

################################ RefClk Location constraints #####################

set_property LOC W7 [get_ports  Q2_CLK1_GTREFCLK_PAD_N_IN ]
set_property LOC W8 [get_ports  Q2_CLK1_GTREFCLK_PAD_P_IN ]

## LOC constrain for DRP_CLK_P/N

set_property LOC G5 [get_ports  SYSCLKP]
set_property LOC G4 [get_ports  SYSCLKN]

##---------- Set placement for gt0_gtx_wrapper_i/GTXE2_CHANNEL ------
set_property LOC GTXE2_CHANNEL_X0Y8 [get_cells gtwizard_0_exdes/gtwizard_0_support_i/gtwizard_0_init_i/U0/gtwizard_0_i/gt0_gtwizard_0_i/gtxe2_i]
##---------- Set placement for gt1_gtx_wrapper_i/GTXE2_CHANNEL ------
set_property LOC GTXE2_CHANNEL_X0Y12 [get_cells gtwizard_0_exdes/gtwizard_0_support_i/gtwizard_0_init_i/U0/gtwizard_0_i/gt1_gtwizard_0_i/gtxe2_i]

################################## Clock Constraints ##########################


# User Clock Constraints
create_clock -period 5.0 [get_pins -filter {REF_PIN_NAME=~*TXOUTCLK} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 6.25 [get_pins -filter {REF_PIN_NAME=~*TXOUTCLKFABRIC} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 6.25 [get_pins -filter {REF_PIN_NAME=~*RXOUTCLKFABRIC} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 5.0 [get_pins -filter {REF_PIN_NAME=~*RXOUTCLK} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]

#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_ports SYSCLK_IN]] -to [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*TXOUTCLK}]]
#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*TXOUTCLK}]] -to [get_clocks -include_generated_clocks -of_objects [get_ports SYS\CLK_IN]]

#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_ports SYSCLK_IN]] -to [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*RXO\UTCLK}]]
#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*RXOUTCLK}]] -to [get_clocks -include_generated_clocks -of_objects [get_ports SYS\CLK_IN]]
# User Clock Constraints

create_clock -period 5.0 [get_pins -filter {REF_PIN_NAME=~*TXOUTCLK} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 6.25 [get_pins -filter {REF_PIN_NAME=~*TXOUTCLKFABRIC} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 6.25 [get_pins -filter {REF_PIN_NAME=~*RXOUTCLKFABRIC} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 5.0 [get_pins -filter {REF_PIN_NAME=~*RXOUTCLK} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt0_gtwizard_0_i*gtxe2_i*}]]
#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_ports SYSCLK_IN]] -to [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*TXOUTCLK}]]
#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*TXOUTCLK}]] -to [get_clocks -include_generated_clocks -of_objects [get_ports SYSCLK_IN]]

#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_ports SYSCLK_IN]] -to [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*RXOUTCLK}]]
#set_false_path -from [get_clocks -include_generated_clocks -of_objects [get_pins -hier -filter {name=~*gt0_gtwizard_0_i*gtxe2_i*RXOUTCLK}]] -to [get_clocks -include_generated_clocks -of_objects [get_ports SYSCLK_IN]]

create_clock -period 5.0 [get_pins -filter {REF_PIN_NAME=~*TXOUTCLK} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt1_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 6.25 [get_pins -filter {REF_PIN_NAME=~*TXOUTCLKFABRIC} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt1_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 6.25 [get_pins -filter {REF_PIN_NAME=~*RXOUTCLKFABRIC} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt1_gtwizard_0_i*gtxe2_i*}]]
create_clock -period 5.0 [get_pins -filter {REF_PIN_NAME=~*RXOUTCLK} -of_objects [get_cells -hierarchical -filter {NAME =~ *gt1_gtwizard_0_i*gtxe2_i*}]]


set_false_path -to [get_cells -hierarchical -filter {NAME =~ *data_sync_reg1}]

set_property PACKAGE_PIN AC3 [get_ports GTXRXN0]
set_property PACKAGE_PIN AC4 [get_ports GTXRXP0]
set_property PACKAGE_PIN AB1 [get_ports GTXTXN0]
set_property PACKAGE_PIN AB2 [get_ports GTXTXP0]

set_property PACKAGE_PIN V5 [get_ports GTXRXN1]
set_property PACKAGE_PIN V6 [get_ports GTXRXP1]
set_property PACKAGE_PIN T1 [get_ports GTXTXN1]
set_property PACKAGE_PIN T2 [get_ports GTXTXP1]


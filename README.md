# GTX_8Gbps_160MHz_Fixed_Latency_from_scratch
長坂によるGTXのマルチレーンの8Gbpsのチュートリアル。
今回はJATHubのGTX2レーンの実装を成川と行った。

## プロジェクトの立ち上げ方。
src/とcon/があるところに移動。
```
cd gtx_8gbps_160mhz_fixed_latency_from_scratch/GTX_8Gbps_160MHz_2lane_FixedLatency
```
prj/ディレクトリを作る。
```
mkdir prj
```
prj/へ移動。
```
cd prj/
```
vivadoを立ち上げる。
```
vivado
```
vvivadoのGUI上でソースファイルとして
```
gtx_8gbps_160mhz_fixed_latency_from_scratch/GTX_8Gbps_160MHz_2lane_FixedLatency/src
```
にあるもの全てを選択。
コンストレインファイルとして、
```
gtx_8gbps_160mhz_fixed_latency_from_scratch/GTX_8Gbps_160MHz_2lane_FixedLatency/con
```
にあるもの全てを選択。FPGAの種類としては、xc7z045ffg900-2を選択する。

## example designの生成
vivadoのipコアジェネレータを開き、ipカタログでgt transceiverを選択。
### GT selection
![](image/GTselection.png)
### Line rate, Refclk selection
![](image/Linerate.png)
### Encoding and Clocking
![](image/Encoding1.png)
![](image/Encoding2.png)
### Comma Alignment and Equalization 
![](image/CommaAlignment1.png)
![](image/CommaAlignment2.png)
### PCLe, SATA, PRBS
![](image/PCLe1.png)
![](image/PCLe2.png)
### CB and CC Sequence
![](image/CB.png)
### summary 
![](image/summary.png)

## いくつかファイルを書き換え。

### gtwizard_0_exdesの書き換え。
奥村さんのgit(https://gitlab.cern.ch/okumura/kc705-gtx-demo-2022 )に従い、書き換える。
マルチレーンであることと、JATHubはSFP＋0のレーンのみTX POLARITYが反転しているので外に信号線を引っ張るようにGUIで設定しているので、そこを注意。

### gtwizard_0_gtの書き換え。
奥村さんのgit(https://gitlab.cern.ch/okumura/kc705-gtx-demo-2022 )に従い、書き換える。

- RXSLIDE_MODEをPMAに変更。
- RXBUF_ENをFALSEに変更。
- RX8B10BENをFALSEに変更。

### gtwizard_0_gt_userclk_sourceの書き換え。
奥村さんのgitのには無い部分。
マルチレーンの場合、TXUSERCLKは共通で良いが、RXUSERCLKはポートごとにやるべきなので、そこを修正する。

まず信号線を定義して、CLK BUFを内部で定義する。
その後それぞれのuserclkに適切な信号線をアサインする。

## tclファイルとxmlファイルから、vhdlのコードだけで動かすために必要なファイルをリストする。

奥村さんのgitを参考にしてやるが、いくつか異なるファイルを持って来ないといけないので、必ず自分で、tclファイルxmlから必要なファイルを持ってくること。

.vhdと.xdcファイルが必要。

## 必要なファイルを持ってきて、topを追加。
必要なファイルでgtwizardは動くようになっているので、それに対応するI/Oをtopで正しく定義する。


